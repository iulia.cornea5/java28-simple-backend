package com.evm.dto.enums;

public enum EventDTOType {
    CONCERT, SPORTS, THEATRE, PARTY
}
