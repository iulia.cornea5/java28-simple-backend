package com.evm.dto;

import com.evm.dto.enums.EventDTOType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@Getter
public class EventDTO {

    private UUID id;
    private String name;
    private EventDTOType eventDTOType;
    //  Compatible formats from String "yyyy-MM-dd'T'HH:mm:ss.SSSX", "yyyy-MM-dd'T'HH:mm:ss.SSS", "EEE, dd MMM yyyy HH:mm:ss zzz", "yyyy-MM-dd"
    private Date startDate;
}
