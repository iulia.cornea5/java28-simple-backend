package com.evm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@Getter
public class UserDTO {

    private UUID uuid;
    private String email;
    private String firstName;
    private String fastName;
    private Date registrationDate;
}
