package com.evm.mapper;

import com.evm.dto.EventDTO;
import com.evm.entity.Event;

public interface EventMapper {

    Event toEntity(EventDTO dto);

    EventDTO toDto(Event entity);
}
