package com.evm.mapper.impl;

import com.evm.dto.EventDTO;
import com.evm.dto.enums.EventDTOType;
import com.evm.entity.Event;
import com.evm.entity.enums.EventType;
import com.evm.mapper.EventMapper;
import org.springframework.stereotype.Component;

@Component
public class EventMapperImpl implements EventMapper {

    @Override
    public Event toEntity(EventDTO dto) {
        return new Event(
                dto.getId(),
                dto.getName(),
                mapType(dto.getEventDTOType()),
                dto.getStartDate());
    }

    @Override
    public EventDTO toDto(Event entity) {
        return new EventDTO(
                entity.getId(),
                entity.getName(),
                mapType(entity.getEventType()),
                entity.getStartDate());
    }

    private EventType mapType(EventDTOType dtoType) {
        if (dtoType == null) {
            return null;
        }
        return EventType.valueOf(dtoType.toString());
    }

    private EventDTOType mapType(EventType eventType) {
        if (eventType == null) {
            return null;
        }
        return EventDTOType.valueOf(eventType.toString());
    }


}
