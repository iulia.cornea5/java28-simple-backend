package com.evm.mapper.impl;

import com.evm.dto.UserDTO;
import com.evm.dto.UserLoginDTO;
import com.evm.entity.User;
import com.evm.mapper.UserMapper;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public User toEntity(UserLoginDTO loginDTO) {
        return new User(
                null,
                loginDTO.getEmail(),
                loginDTO.getPassword(),
                null,
                null,
                new Date(System.currentTimeMillis())
        );
    }

    @Override
    public UserDTO toDTO(User entity) {
        return new UserDTO(
                entity.getUuid(),
                entity.getEmail(),
                entity.getFirstName(),
                entity.getLastName(),
                entity.getRegistrationDate()
        );
    }
}
