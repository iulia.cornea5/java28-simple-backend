package com.evm.mapper;

import com.evm.dto.UserDTO;
import com.evm.dto.UserLoginDTO;
import com.evm.entity.User;

public interface UserMapper {

    User toEntity(UserLoginDTO loginDTO);

    UserDTO toDTO(User entity);
}
