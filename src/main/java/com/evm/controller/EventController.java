package com.evm.controller;

import com.evm.dto.EventDTO;
import com.evm.service.EventService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/events")
public class EventController {

    private EventService service;

    public EventController(EventService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<EventDTO>> getAll() {
        return new ResponseEntity<List<EventDTO>>(service.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<EventDTO> create(@RequestBody EventDTO eventDTO) {
        return new ResponseEntity<EventDTO>(service.saveEvent(eventDTO), HttpStatus.OK);
    }

    @PostMapping("/{uuid}")
    public ResponseEntity<EventDTO> update(@PathVariable String uuid, @RequestBody EventDTO eventDTO) {
        // TODO discuss how to implement
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<UUID> delete(@PathVariable String uuid) {
        // TODO discuss how to implement
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
