package com.evm.controller;

import com.evm.dto.UserDTO;
import com.evm.dto.UserLoginDTO;
import com.evm.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {

        this.userService = userService;
    }

    // TODO discuss authorization
    @GetMapping
    public ResponseEntity<List<UserDTO>> getAll() {

        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }

    // TODO discuss authorization
    @GetMapping("/{uuid}")
    public ResponseEntity<UserDTO> getOne(@PathVariable String uuid) {
        return new ResponseEntity<>(userService.getByUUID(uuid), HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<UserDTO> create(@RequestBody UserLoginDTO loginDTO) {
        return new ResponseEntity<>(userService.signUp(loginDTO), HttpStatus.OK);
    }

    @PostMapping("/{uuid}")
    public ResponseEntity<UserDTO> update(String uuid, @RequestBody UserDTO userDTO) {
        // TODO discuss how to implement
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }


}
