package com.evm.service;

import com.evm.dto.UserDTO;
import com.evm.dto.UserLoginDTO;
import com.evm.exceptions.EmailAddressAlreadyUsedException;
import com.evm.exceptions.InvalidEmailAddressException;
import com.evm.exceptions.ResourceNotFoundException;

import java.util.List;

public interface UserService {
    UserDTO signUp(UserLoginDTO loginDTO) throws InvalidEmailAddressException, EmailAddressAlreadyUsedException;

    List<UserDTO> getAll();

    UserDTO getByUUID(String uuid) throws ResourceNotFoundException;

}
