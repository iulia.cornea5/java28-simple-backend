package com.evm.service;

import com.evm.dto.EventDTO;

import java.util.List;

public interface EventService {

    /**
     * Creates or updates the event.
     * If the id is null, a new entry is added in the database.
     * If the id is not null, the existing entry is updated.
     *
     * @param e event to be created/updated
     * @return the created/updated event
     */
    EventDTO saveEvent(EventDTO e);

    List<EventDTO> findAll();
}
