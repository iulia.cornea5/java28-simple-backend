package com.evm.service.impl;

import com.evm.dto.UserDTO;
import com.evm.dto.UserLoginDTO;
import com.evm.entity.User;
import com.evm.exceptions.EmailAddressAlreadyUsedException;
import com.evm.exceptions.InvalidEmailAddressException;
import com.evm.exceptions.ResourceNotFoundException;
import com.evm.mapper.UserMapper;
import com.evm.repository.UserRepo;
import com.evm.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserRepo repo;
    private UserMapper mapper;

    @Value("${user.validation.email-regex}")
    private String emailRegex = "^(.+)@gmail\\.com"; // default backup regex validation

    public UserServiceImpl(UserRepo repo, UserMapper mapper) {
        this.repo = repo;
        this.mapper = mapper;
    }


    @Override
    public UserDTO signUp(UserLoginDTO loginDTO) throws InvalidEmailAddressException, EmailAddressAlreadyUsedException {
        if (!isValidEmail(loginDTO.getEmail())) {
            throw new InvalidEmailAddressException(loginDTO.getEmail());
        }
        if (repo.existsByEmail(loginDTO.getEmail())) {
            throw new EmailAddressAlreadyUsedException(loginDTO.getEmail());
        }
        User user = repo.save(mapper.toEntity(loginDTO));
        return mapper.toDTO(user);
    }

    @Override
    public List<UserDTO> getAll() {

        return repo.findAll().stream().map(mapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public UserDTO getByUUID(String uuidString) throws ResourceNotFoundException {
        UUID uuid = UUID.fromString(uuidString);
        if (!repo.existsById(uuid)) {
            throw new ResourceNotFoundException("User", "UUID", uuidString);
        }
        return mapper.toDTO(repo.getById(uuid));
    }

    private boolean isValidEmail(String email) {
        return email != null && email.matches(emailRegex);
    }

}
