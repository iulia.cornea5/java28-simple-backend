package com.evm.service.impl;

import com.evm.dto.EventDTO;
import com.evm.mapper.EventMapper;
import com.evm.repository.EventRepo;
import com.evm.service.EventService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {

    private EventRepo repo;
    private EventMapper mapper;

    public EventServiceImpl(EventRepo repo, EventMapper mapper) {
        this.repo = repo;
        this.mapper = mapper;
    }

    @Override
    public EventDTO saveEvent(EventDTO dto) {
//        Event entityToSave = mapper.toEntity(dto);
//        Event savedEntity =  repo.save(entityToSave);
//        return mapper.toDto(savedEntity);
        return mapper.toDto(repo.save(mapper.toEntity(dto)));
    }

    @Override
    public List<EventDTO> findAll() {

        return repo.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }
}
