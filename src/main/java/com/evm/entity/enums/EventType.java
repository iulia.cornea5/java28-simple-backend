package com.evm.entity.enums;

public enum EventType {
    CONCERT, SPORTS, THEATRE, PARTY
}
