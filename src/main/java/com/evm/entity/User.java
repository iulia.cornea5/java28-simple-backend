package com.evm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
// added @Table annotation in order to be able to specify the uniqueness of the email column
@Table(
        name = "application_user",
        uniqueConstraints = @UniqueConstraint(columnNames = {"email"})
)
public class User {

    @Id
    @GeneratedValue
    private UUID uuid;
    @NonNull
    private String email;
    @NonNull
    private String password;
    private String firstName;
    private String lastName;
    private Date registrationDate;
}
