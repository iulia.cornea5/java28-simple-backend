package com.evm.repository;

import com.evm.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepo extends JpaRepository<User, UUID> {

    boolean existsByEmail(String email);
}
